﻿namespace com.mobiquity.packer;
public static class Packer
{
    public static string pack(string filePath)
    {
        var packingService = new PackingService();
        var result = packingService.PackFromFile(filePath);
        return result;
    }
}
