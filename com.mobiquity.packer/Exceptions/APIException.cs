using com.mobiquity.packer.Enums;

namespace com.mobiquity.packer.Exceptions;

public class APIException : Exception
{
    public APIException(ErrorCodes errorCodes, string message)
            : base(message)
    {
        Type = errorCodes;
        Title = errorCodes.GetDescription();
    }

    public APIException(ErrorCodes errorCodes, string message, Exception innerException)
        : base(message, innerException)
    {
        Title = errorCodes.GetDescription();
    }
    public ErrorCodes Type { get; }
    public string Title { get; }
}