using com.mobiquity.packer.Configuration;

namespace com.mobiquity.packer.Entities;

public class PackageItem
{
    private PackageItemOptions _packageItemOptions;
    public int Index { get; protected set; }
    public decimal Weight { get; protected set; }
    public decimal Price { get; protected set; }
    public PackageItem(int index, decimal weight, decimal price)
    {
        Index = index;
        Weight = weight;
        Price = price;
        _packageItemOptions = new PackageItemOptions();
    }

    public bool IsValid()
    {
        var invalidWeight = Weight <= _packageItemOptions.MaxWeight && Weight > 0;
        var invalidPrice = Price <= _packageItemOptions.MaxCost && Price > 0;

        return invalidWeight && invalidPrice;
    }
}