using com.mobiquity.packer.Configuration;
using com.mobiquity.packer.Enums;
using com.mobiquity.packer.Exceptions;

namespace com.mobiquity.packer.Entities;
public class Package
{
    private PackageOptions _packageOptions;
    public decimal Weight { get; protected set; }
    public List<PackageItem> PackedItems { get; protected set; }
    public List<PackageItem> PossibleItems { get; protected set; }
    public Package(decimal weight)
    {
        Weight = weight;
        PackedItems = new List<PackageItem>();
        PossibleItems = new List<PackageItem>();
        _packageOptions = new PackageOptions();
    }

    public void AddPossibleItems(IList<PackageItem> items)
    {
        if (PossibleItems.Any())
            throw new APIException(ErrorCodes.InvalidPackageOperation, $"You have already added {PossibleItems.Count} items");

        var uniqueItemIds = items.Select(x => x.Index).Distinct();
        if (uniqueItemIds.Count() != items.Count)
            throw new APIException(ErrorCodes.InvalidPackageOperation, $"Duplicate items: items with the same index cannot be added twice");
        PossibleItems.AddRange(items);
    }

    public void Pack()
    {
        var invalidWeight = Weight > _packageOptions.MaxWeight || Weight < 0;
        
        if (invalidWeight)
            throw new APIException(ErrorCodes.InvalidPackageOperation, $"A package cannot have a weight of: {Weight}");

        PackedItems = new List<PackageItem>();
        var packingItems = new List<PackageItem>();

        var validPackageItems = PossibleItems
            .Where(x => x.IsValid())
            .OrderByDescending(x => x.Price / x.Weight)
            .ToList();

        foreach (var validPackageItem in validPackageItems)
        {
            var totalAvaliableWeight = Weight - packingItems.Sum(x => x.Weight);

            if (totalAvaliableWeight - validPackageItem.Weight >= 0)
                packingItems.Add(validPackageItem);
        }

        var limit = packingItems.Count() > _packageOptions.MaxItems ? _packageOptions.MaxItems : packingItems.Count();

        PackedItems = packingItems.Take(limit).OrderBy(x => x.Index).ToList();
    }

    public override string ToString()
    {
        var stringResult = string.Join(",", PackedItems.Select(x => x.Index));
        return string.IsNullOrEmpty(stringResult.Trim()) ? "-" : stringResult;
    }
}