using System.ComponentModel;

namespace com.mobiquity.packer.Enums;

public enum ErrorCodes
{
    [Description("File Not Found Error")]
    FileNotFound,
    [Description("File Unreadable Error")]
    FileUnreadable,
    [Description("Argument/parameter is not valid")]
    InvalidArgumentException,
    [Description("Package exceeds system constraints")]
    InvalidPackageOperation,
    [Description("File could not be parsed")]
    FileFormatException
}