using System.Reflection;

namespace com.mobiquity.packer.Enums;

internal static class EnumExtensions
{
    internal static string GetDescription(this Enum genericEnum)
    {
        Type enumType = genericEnum.GetType();
        MemberInfo[] memberInformation = enumType.GetMember(genericEnum.ToString());

        var attributes = memberInformation[0].GetCustomAttributes(typeof(System.ComponentModel.DescriptionAttribute), false);
        if (attributes.Any())
        {
            return ((System.ComponentModel.DescriptionAttribute)attributes.ElementAt(0)).Description;
        }

        return genericEnum.ToString();
    }
}