using com.mobiquity.packer.AbstractFactory;
using com.mobiquity.packer.Entities;
using com.mobiquity.packer.Utils;

namespace com.mobiquity.packer;
public class PackingService : IPackingService
{
    private readonly IPackageFactory _packageFactory;
    public PackingService()
    {
        _packageFactory = new PackageFromFileStringFactory();
    }

    public string PackFromFile(string filePath)
    {
        var content = FileUtils.ReadUtfFile(filePath);

        var packageStringItems = _packageFactory.SplitPackages(content);
        var packages = new List<Package>();
        
        foreach (var packageStringItem in packageStringItems)
        {
            var package = _packageFactory.GetPackage(packageStringItem);
            var packageItems = _packageFactory.GetPackageItems(packageStringItem);

            package.AddPossibleItems(packageItems);

            package.Pack();

            packages.Add(package);
        }
        var packageStrings = packages.Select(x => x.ToString());

        return string.Join("\n", packageStrings);
    }
}