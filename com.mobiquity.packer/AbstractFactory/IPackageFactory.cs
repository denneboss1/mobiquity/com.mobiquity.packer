using com.mobiquity.packer.Entities;

namespace com.mobiquity.packer.AbstractFactory;

///Abstract factury is implemented here in order to construct the objects using "complex" input
public interface IPackageFactory
{
    IList<string> SplitPackages(string packagesContent);
    Package GetPackage(string content);
    IList<PackageItem> GetPackageItems(string content);
}