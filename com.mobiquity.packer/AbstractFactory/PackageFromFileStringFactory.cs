using System.Globalization;
using com.mobiquity.packer.Configuration;
using com.mobiquity.packer.Entities;
using com.mobiquity.packer.Enums;
using com.mobiquity.packer.Exceptions;

namespace com.mobiquity.packer.AbstractFactory;
public class PackageFromFileStringFactory : IPackageFactory
{
    private readonly CultureInfoOptions _cultureInfoOptions;
    public PackageFromFileStringFactory()
    {
        _cultureInfoOptions = new CultureInfoOptions();
    }
    public IList<string> SplitPackages(string packagesContent)
    {
        var packageStrings = packagesContent
            .Split('\n')
            .Where(x => !string.IsNullOrEmpty(x?.Trim()))
            .ToList();

        if (!packageStrings.Any())
            throw new APIException(ErrorCodes.FileFormatException, $"Empty file provided");

        foreach (var packageString in packageStrings)
        {
            var colon = packageString.IndexOf(':');
            if (colon <= 0)
                throw new APIException(ErrorCodes.FileFormatException, $"Invalid file format, no `:` found");
            var itemsString = packageString.Substring(colon + 1);
            if (itemsString.Contains(':'))
                throw new APIException(ErrorCodes.FileFormatException, $"Invalid file format. More than one package per line.");
            var openBrackets = itemsString.Count(x => x.CompareTo('(') == 0);
            var closeBrackets = itemsString.Count(x => x.CompareTo(')') == 0);
            var delimeterCount = itemsString.Count(x => x.CompareTo(',') == 0);
            if (openBrackets != closeBrackets)
                throw new APIException(ErrorCodes.FileFormatException, $"Invalid file format: no matching open/close backets");

            if (delimeterCount / openBrackets != 2)
                throw new APIException(ErrorCodes.FileFormatException, $"Invalid file format: each item requires 3 fields");

        }

        return packageStrings;
    }
    public Package GetPackage(string content)
    {
        var weightString = content.Split(":").FirstOrDefault()?.Trim();
        if (string.IsNullOrEmpty(weightString))
            throw new APIException(ErrorCodes.InvalidArgumentException, $"Could not determine the weight for the package in \"{content}\"");
        decimal weight;
        var success = Decimal.TryParse(weightString, out weight);
        if (!success)
            throw new APIException(ErrorCodes.InvalidArgumentException, $"The weight provided is not valid for a Package: \"{weightString}\"");
        return new Package(weight);
    }

    private (int, decimal, decimal) GetPackageItemValues(string strIndex, string strWeight, string strPrice)
    {
        int index;
        var success = int.TryParse(strIndex, NumberStyles.AllowLeadingWhite, _cultureInfoOptions.GetCulture, out index);
        if (!success)
            throw new APIException(ErrorCodes.InvalidArgumentException, $"The index provided is not valid for a Package Item: \"{strIndex}\"");

        decimal weight;
        success = decimal.TryParse(strWeight, NumberStyles.AllowDecimalPoint, _cultureInfoOptions.GetCulture, out weight);
        if (!success)
            throw new APIException(ErrorCodes.InvalidArgumentException, $"The weight provided is not valid for a Package Item: \"{strWeight}\"");

        decimal price;
        success = decimal.TryParse(strPrice, NumberStyles.AllowCurrencySymbol, _cultureInfoOptions.GetCulture, out price);
        if (!success)
            throw new APIException(ErrorCodes.InvalidArgumentException, $"The price provided is not valid for a Package Item: \"{strPrice}\"");

        return (index, weight, price);
    }

    public IList<PackageItem> GetPackageItems(string content)
    {
        var packageItems = new List<PackageItem>();
        var packageItemsString = content.Split(":").LastOrDefault()?.Trim();
        if (string.IsNullOrEmpty(packageItemsString))
            throw new APIException(ErrorCodes.InvalidArgumentException, $"Could locate any Package Items in \"{content}\"");
        var packageItemStrings = packageItemsString.Split(')');

        foreach (var packageItemString in packageItemStrings)
        {
            var packageItemCleanString = packageItemString.Replace("(", " ").Trim();
            if (string.IsNullOrEmpty(packageItemCleanString))
                continue;

            var packageItemStringValues = packageItemCleanString.Split(',');

            if (packageItemStringValues.Length != 3)
                throw new APIException(ErrorCodes.InvalidArgumentException, $"Package item values could not be determined: \"{packageItemString}\"");

            var (index, weight, price) = GetPackageItemValues(packageItemStringValues[0], packageItemStringValues[1], packageItemStringValues[2]);

            packageItems.Add(new PackageItem(index, weight, price));
        }

        return packageItems;
    }
}