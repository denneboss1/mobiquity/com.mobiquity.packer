using System.Globalization;

namespace com.mobiquity.packer.Configuration;

public class CultureInfoOptions
{
    public CultureInfo GetCulture { get; } = new CultureInfo("en-IE");
}