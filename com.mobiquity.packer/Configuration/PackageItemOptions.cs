namespace com.mobiquity.packer.Configuration;

public class PackageItemOptions
{
    public decimal MaxCost { get; } = 100M;
    public decimal MaxWeight { get; } = 100M;
}