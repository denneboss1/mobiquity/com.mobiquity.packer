namespace com.mobiquity.packer.Configuration;

public class PackageOptions
{
    public decimal MaxWeight { get; } = 100M;
    public int MaxItems { get; } = 15;
}