namespace com.mobiquity.packer;
public interface IPackingService
{
    string PackFromFile(string filePath);
}