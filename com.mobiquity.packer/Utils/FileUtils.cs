using System.Text;
using com.mobiquity.packer.Enums;
using com.mobiquity.packer.Exceptions;

namespace com.mobiquity.packer.Utils;
public static class FileUtils
{
    public static string ReadUtfFile(string filePath)
    {
        if (string.IsNullOrEmpty(filePath))
        {
            throw new APIException(ErrorCodes.FileNotFound, "Empty file path provided.");
        }

        var platformPathBuilder = new StringBuilder(filePath);
        if (filePath.Contains("\\"))
        {
            var pathParts = filePath.Split("\\");
            platformPathBuilder = new StringBuilder(string.Join(Path.AltDirectorySeparatorChar.ToString(), pathParts));
        }

        if (!File.Exists(platformPathBuilder.ToString()))
        {
            throw new APIException(ErrorCodes.FileNotFound, $"File not found at path: {platformPathBuilder.ToString()}");
        }

        try
        {
            string content = File.ReadAllText(platformPathBuilder.ToString());
            return content;
        }
        catch (IOException ex)
        {
            throw new APIException(ErrorCodes.FileUnreadable, $"Error in reading file ({platformPathBuilder.ToString()}) : {ex.Message}", ex);
        }
    }
}