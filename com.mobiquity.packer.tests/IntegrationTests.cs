using com.mobiquity.packer.Enums;
using com.mobiquity.packer.Exceptions;

namespace com.mobiquity.packer.tests;

public class IntegrationTests : IDisposable
{
    private const string EmptyContentTestFilePath = "empty-test-file-input.txt";
    private const string TestFileExpectedResult = "4\n-\n2,3,4\n8,9";
    private const string TestFilePath = "test-file-input.txt";
    private const string TestFileContent = @"81 : (1,53.38,€45) (2,88.62,€98) (3,78.48,€3) (4,72.30,€76) (5,30.18,€9) (6,46.34,€48)
8 : (1,15.3,€34)
75 : (1,85.31,€29) (2,14.55,€74) (3,3.98,€16) (4,26.24,€55) (5,63.69,€52) (6,76.25,€75) (7,60.02,€74) (8,93.18,€35) (9,89.95,€78)
56 : (1,90.72,€13) (2,33.80,€40) (3,43.15,€10) (4,37.97,€16) (5,46.81,€36) (6,48.77,€79) (7,81.80,€45) (8,19.36,€79) (9,6.76,€64)";
    private const string TestFilePath2 = "test-file-input2.txt";
    private const string TestFileContent2 = "10:(1,1,1)(2,2,2)\n12:(3,4,5)\n1:(1,200,3)";
    public IntegrationTests()
    {
        File.WriteAllText(TestFilePath, TestFileContent);
        File.WriteAllText(TestFilePath2, TestFileContent2);
        File.WriteAllText(EmptyContentTestFilePath, "");
    }

    public void Dispose()
    {
        if (File.Exists(TestFilePath))
        {
            File.Delete(TestFilePath);
        }
        if (File.Exists(TestFilePath2))
        {
            File.Delete(TestFilePath2);
        }
        if (File.Exists(EmptyContentTestFilePath))
        {
            File.Delete(EmptyContentTestFilePath);
        }
    }

    /* 
        I could be missing the plot here :-) but my take on the assignment (given the context and available parameters) is:
        1. Cost = expense
        2. Price = profit
        Therefore I feel that packing 2, 3 and 4 is more profitable to ship than 2 and 7
2 and 7:
2	14.55	74
7	60.02	74
		
	74.57	148

2, 3 and 4:
2	14.55	74
3	3.98	16
4	26.24	55
		
	44.77	145
*/

    [Fact]
    public void pack_AssignmentContentInput_ReturnsAssignmentContentOutput()
    {
        FileInfo fi = new FileInfo(TestFilePath);

        var result = Packer.pack(fi.FullName);
        Assert.Equal(TestFileExpectedResult, result);
    }

    [Fact]
    public void pack_TestFileContent2_Returns1And2Then3ThenDash()
    {
        FileInfo fi = new FileInfo(TestFilePath2);

        var result = Packer.pack(fi.FullName);
        Assert.Equal("1,2\n3\n-", result);
    }

    [Fact]
    public void pack_ReplaceFilePathWithBackSlash_ReturnsAssignmentContentOutput()
    {
        FileInfo fi = new FileInfo(TestFilePath);
        var windowsFileParts = fi.FullName.Split(Path.DirectorySeparatorChar);
        var windowsFilePath = string.Join("\\", windowsFileParts);

        var result = Packer.pack(windowsFilePath);

        Assert.Equal(TestFileExpectedResult, result);
    }

    [Fact]
    public void pack_EmptyFile_ReturnsNfileNotFoundException()
    {
        FileInfo fi = new FileInfo(EmptyContentTestFilePath);

        var exception = Assert.Throws<APIException>(() => Packer.pack(fi.FullName));

        Assert.Equal<ErrorCodes>(ErrorCodes.FileFormatException, exception.Type);
        Assert.Equal("Empty file provided", exception.Message);
    }
}