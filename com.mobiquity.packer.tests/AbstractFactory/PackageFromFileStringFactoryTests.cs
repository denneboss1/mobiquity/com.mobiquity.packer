using com.mobiquity.packer.AbstractFactory;
using com.mobiquity.packer.Entities;
using com.mobiquity.packer.Enums;
using com.mobiquity.packer.Exceptions;

namespace com.mobiquity.packer.tests.AbstractFactory;

public class PackageFromFileStringFactoryTests
{
    private readonly IPackageFactory _packageFactory;
    private const string ExampleFileContent = @"81 : (1,53.38,€45) (2,88.62,€98) (3,78.48,€3) (4,72.30,€76) (5,30.18,€9) (6,46.34,€48)
8 : (1,15.3,€34)
75 : (1,85.31,€29) (2,14.55,€74) (3,3.98,€16) (4,26.24,€55) (5,63.69,€52) (6,76.25,€75) (7,60.02,€74) (8,93.18,€35) (9,89.95,€78)
56 : (1,90.72,€13) (2,33.80,€40) (3,43.15,€10) (4,37.97,€16) (5,46.81,€36) (6,48.77,€79) (7,81.80,€45) (8,19.36,€79) (9,6.76,€64)";

    public PackageFromFileStringFactoryTests()
    {
        _packageFactory = new PackageFromFileStringFactory();
    }
    [Fact]
    public void SplitPackages_EmptyFile_ReturnFileFormatException()
    {
        var errorContent = @"";
        var exception = Assert.Throws<APIException>(() => _packageFactory.SplitPackages(errorContent));

        Assert.Equal<ErrorCodes>(ErrorCodes.FileFormatException, exception.Type);
        Assert.StartsWith("Empty file provided", exception.Message);
    }

    [Fact]
    public void SplitPackages_MissingColon_ReturnFileFormatException()
    {
        var errorContent = @"81 (1,2,3)";
        var exception = Assert.Throws<APIException>(() => _packageFactory.SplitPackages(errorContent));

        Assert.Equal<ErrorCodes>(ErrorCodes.FileFormatException, exception.Type);
        Assert.Equal("Invalid file format, no `:` found", exception.Message);
    }

    [Fact]
    public void SplitPackages_MissingWeight_ReturnFileFormatException()
    {
        var errorContent = @": (1,2,3)";
        var exception = Assert.Throws<APIException>(() => _packageFactory.SplitPackages(errorContent));

        Assert.Equal<ErrorCodes>(ErrorCodes.FileFormatException, exception.Type);
        Assert.Equal("Invalid file format, no `:` found", exception.Message);
    }

    [Fact]
    public void SplitPackages_MissingOpenBracket_ReturnFileFormatException()
    {
        var errorContent = @"81 : (1,2,3) 1,2,3) (1,2,3)";
        var exception = Assert.Throws<APIException>(() => _packageFactory.SplitPackages(errorContent));

        Assert.Equal<ErrorCodes>(ErrorCodes.FileFormatException, exception.Type);
        Assert.Equal("Invalid file format: no matching open/close backets", exception.Message);
    }

    [Fact]
    public void SplitPackages_MissingCloseBracket_ReturnFileFormatException()
    {
        var errorContent = @"81 : (1,2,3) (1,2,3) (1,2,3";
        var exception = Assert.Throws<APIException>(() => _packageFactory.SplitPackages(errorContent));

        Assert.Equal<ErrorCodes>(ErrorCodes.FileFormatException, exception.Type);
        Assert.Equal("Invalid file format: no matching open/close backets", exception.Message);
    }

    [Fact]
    public void SplitPackages_MissingField_ReturnFileFormatException()
    {
        var errorContent = @"81 : (1,3) (1,2,3) (1,2,3)";
        var exception = Assert.Throws<APIException>(() => _packageFactory.SplitPackages(errorContent));

        Assert.Equal<ErrorCodes>(ErrorCodes.FileFormatException, exception.Type);
        Assert.Equal("Invalid file format: each item requires 3 fields", exception.Message);
    }

    [Fact]
    public void SplitPackages_MissingNewLines_ReturnFileFormatException()
    {
        var errorContent = "81 : (1,53.38,$45) (2,88.62,$98) (3,78.48,$3) (4,72.30,$76) (5,30.18,$9) (6,46.34,$48) 8 : (1,15.3,$34)";
        var exception = Assert.Throws<APIException>(() => _packageFactory.SplitPackages(errorContent));

        Assert.Equal<ErrorCodes>(ErrorCodes.FileFormatException, exception.Type);
        Assert.Equal("Invalid file format. More than one package per line.", exception.Message);
    }

     [Fact]
    public void SplitPackages_OneItemWithBlankLines_ReturnOne()
    {
        var errorContent = "\n\n\n81 : (1,3, 3) (1,2,3) (1,2,3)\n\n\n";
        var result = _packageFactory.SplitPackages(errorContent);

        var count = result.Count();

        Assert.Equal<int>(1, count);
    }

    [Fact]
    public void GetPackageItems_InvalidCurrencyContent_ReturnInvalidArgumentException()
    {
        var errorContent = @"81 : (1,53.38,$45) (2,88.62,$98) (3,78.48,$3) (4,72.30,$76) (5,30.18,$9) (6,46.34,$48)
8 : (1,15.3,$34)";
        var packageStrings = _packageFactory.SplitPackages(errorContent);

        var exception = Assert.Throws<APIException>(() => _packageFactory.GetPackageItems(packageStrings.First()));

        Assert.Equal<ErrorCodes>(ErrorCodes.InvalidArgumentException, exception.Type);
    }

    [Fact]
    public void GetPackageItems_InvalidWeightDecimal_ReturnInvalidArgumentException()
    {
        var errorContent = @"8 : (1,15'3,€34)";
        var packageStrings = _packageFactory.SplitPackages(errorContent);

        var exception = Assert.Throws<APIException>(() => _packageFactory.GetPackageItems(packageStrings.First()));

        Assert.Equal<ErrorCodes>(ErrorCodes.InvalidArgumentException, exception.Type);
    }

    [Fact]
    public void GetPackageItems_InvalidIndex_ReturnInvalidArgumentException()
    {
        var errorContent = @"12 : (a,15.3,€34)";
        var packageStrings = _packageFactory.SplitPackages(errorContent);

        var exception = Assert.Throws<APIException>(() => _packageFactory.GetPackageItems(packageStrings.First()));

        Assert.Equal<ErrorCodes>(ErrorCodes.InvalidArgumentException, exception.Type);
    }

    [Fact]
    public void GetPackage_BlankWeight_ReturnInvalidArgumentException()
    {
        var errorContent = @" : (1,15.3,€34)";
        var packageStrings = _packageFactory.SplitPackages(errorContent);

        var exception = Assert.Throws<APIException>(() => _packageFactory.GetPackage(packageStrings.First()));

        Assert.Equal<ErrorCodes>(ErrorCodes.InvalidArgumentException, exception.Type);
    }

    [Fact]
    public void GetPackage_NonNumericWeight_ReturnInvalidArgumentException()
    {
        var errorContent = @"x : (1,15.3,€34)";
        var packageStrings = _packageFactory.SplitPackages(errorContent);

        var exception = Assert.Throws<APIException>(() => _packageFactory.GetPackage(packageStrings.First()));

        Assert.Equal<ErrorCodes>(ErrorCodes.InvalidArgumentException, exception.Type);
    }

    [Fact]
    public void GetPackage_ExampleFileContent_ReturnAllPackagesWithFirstWeight81AndLastWeightd56()
    {
        var packageStrings = _packageFactory.SplitPackages(ExampleFileContent);

        var packages = new List<Package>();
        foreach (var packageString in packageStrings)
            packages.Add(_packageFactory.GetPackage(packageString));
        Assert.Equal<decimal>(81M, packages.First().Weight);
        Assert.Equal<decimal>(56M, packages.Last().Weight);
    }

    [Fact]
    public void GetPackage_ExampleFileContent_ReturnFourPackages()
    {
        var packageStrings = _packageFactory.SplitPackages(ExampleFileContent);

        var packages = new List<Package>();
        foreach (var packageString in packageStrings)
            packages.Add(_packageFactory.GetPackage(packageString));
        Assert.Equal<int>(4, packages.Count);
    }

    [Fact]
    public void GetPackageItems_ExampleFileContent_ReturnFileCount25()
    {
        var packageItemStrings = _packageFactory.SplitPackages(ExampleFileContent);

        var packageItems = new List<PackageItem>();
        foreach (var packageItemString in packageItemStrings)
            packageItems.AddRange(_packageFactory.GetPackageItems(packageItemString));

        Assert.Equal<int>(25, packageItems.Count);
    }

    [Fact]
    public void GetPackageItems_ExampleFileContent_ReturnFileTotalAmount1183()
    {
        var packageItemStrings = _packageFactory.SplitPackages(ExampleFileContent);

        var packageItems = new List<PackageItem>();
        foreach (var packageItemString in packageItemStrings)
            packageItems.AddRange(_packageFactory.GetPackageItems(packageItemString));

        Assert.Equal<decimal>(1183M, packageItems.Sum(x => x.Price));
    }

    [Fact]
    public void GetPackageItems_ExampleFileContent_ReturnFileTotalWeight1306_91()
    {
        var packageItemStrings = _packageFactory.SplitPackages(ExampleFileContent);

        var packageItems = new List<PackageItem>();
        foreach (var packageItemString in packageItemStrings)
            packageItems.AddRange(_packageFactory.GetPackageItems(packageItemString));

        Assert.Equal<decimal>(1306.91M, packageItems.Sum(x => x.Weight));
    }
}