using com.mobiquity.packer.Entities;

namespace com.mobiquity.packer.tests.Entities;

public class PackageItemUnitTests
{
    public PackageItemUnitTests()
    {
    }

    [Theory]
    [InlineData(1, 100, 100)]
    [InlineData(2, 10, 10)]
    [InlineData(3, 99, 1)]
    [InlineData(4, 5, 99)]
    public void IsValid_Weight_ReturnsCorrectWeight(int index, decimal weight, decimal price)
    {
        var packageItem = new PackageItem(index, weight, price);
        Assert.Equal<decimal>(weight, packageItem.Weight);
    }

    [Theory]
    [InlineData(1, 100, 100)]
    [InlineData(2, 10, 10)]
    [InlineData(3, 99, 1)]
    [InlineData(4, 5, 99)]
    public void IsValid_Index_ReturnsCorrectIndex(int index, decimal weight, decimal price)
    {
        var packageItem = new PackageItem(index, weight, price);
        Assert.Equal<decimal>(index, packageItem.Index);
    }

    [Theory]
    [InlineData(1, 100, 100)]
    [InlineData(2, 10, 10)]
    [InlineData(3, 99, 1)]
    [InlineData(4, 5, 99)]
    public void IsValid_Price_ReturnsCorrectPrice(int index, decimal weight, decimal price)
    {
        var packageItem = new PackageItem(index, weight, price);
        Assert.Equal<decimal>(price, packageItem.Price);
    }

    [Theory]
    [InlineData(1, 100, 100)]
    [InlineData(2, 10, 10)]
    [InlineData(3, 99, 1)]
    [InlineData(4, 5, 99)]
    public void IsValid_ValidWeightAndCost_ReturnsTrue(int index, decimal weight, decimal price)
    {
        var packageItem = new PackageItem(index, weight, price);
        Assert.True(packageItem.IsValid());
    }

    [Theory]
    [InlineData(1, 101, 101)]
    [InlineData(2, -1, -10)]
    [InlineData(3, 0, 0)]
    public void IsValid_ValidWeightAndCost_ReturnsFalse(int index, decimal weight, decimal price)
    {
        var packageItem = new PackageItem(index, weight, price);
        Assert.False(packageItem.IsValid());
    }
}