using com.mobiquity.packer.Entities;
using com.mobiquity.packer.Enums;
using com.mobiquity.packer.Exceptions;

namespace com.mobiquity.packer.tests.Entities;

public class PackageUnitTests
{

    public PackageUnitTests()
    {
    }

    [Fact]
    public void AddPossibleItems_ValidItems_ReturnsCount()
    {
        var package = new Package(80);
        var packageItems = new List<PackageItem>()
        {
            new PackageItem(1, 2, 3),
            new PackageItem(2, 3, 2),
            new PackageItem(3, 3, 3)
        };
        package.AddPossibleItems(packageItems);

        Assert.Equal<int>(3, package.PossibleItems.Count());
    }

    [Fact]
    public void AddPossibleItems_DuplicateItems_ThrowsInvalidPackageOperationException()
    {
        var package = new Package(80);
        var packageItems = new List<PackageItem>()
        {
            new PackageItem(1, 100, 100),
            new PackageItem(1, 10, 10)
        };
        var exception = Assert.Throws<APIException>(() => package.AddPossibleItems(packageItems));

        Assert.Equal<ErrorCodes>(ErrorCodes.InvalidPackageOperation, exception.Type);
        Assert.Equal("Duplicate items: items with the same index cannot be added twice", exception.Message);
    }

    [Fact]
    public void AddPossibleItems_AddPossibleItemsTwice_ThrowsInvalidPackageOperationException()
    {
        var package = new Package(80);
        var packageItems = new List<PackageItem>()
        {
            new PackageItem(1, 100, 100),
            new PackageItem(2, 10, 10)
        };
        package.AddPossibleItems(packageItems);
        packageItems = new List<PackageItem>()
        {
            new PackageItem(5, 9, 2),
            new PackageItem(6, 1, 12)
        };
        var exception = Assert.Throws<APIException>(() => package.AddPossibleItems(packageItems));

        Assert.Equal<ErrorCodes>(ErrorCodes.InvalidPackageOperation, exception.Type);
        Assert.Equal("You have already added 2 items", exception.Message);
    }

    [Fact]
    public void AddPossibleItems_AddTwoToMaxWeight_Returns2()
    {
        var package = new Package(80);
        var packageItems = new List<PackageItem>()
        {
            new PackageItem(1, 20, 100),
            new PackageItem(2, 20, 1)
        };
        package.AddPossibleItems(packageItems);

        Assert.Equal<int>(2, package.PossibleItems.Count());
    }

    [Fact]
    public void Pack_AddTwoOverLimitItemsExceedingPackageWeight_Returns1PackedItem()
    {
        var package = new Package(79);
        var packageItems = new List<PackageItem>()
        {
            new PackageItem(1, 40, 1),
            new PackageItem(2, 40, 1)
        };
        package.AddPossibleItems(packageItems);
        package.Pack();

        var count = package.PackedItems.Count();

        Assert.Equal<int>(1, count);
    }
    [Fact]
    public void Pack_AddTwoOverLimitItemsWithSameWeightAndDifferentPrices_ReturnsMostExpensivePackedItem()
    {
        var package = new Package(79);
        var packageItems = new List<PackageItem>()
        {
            new PackageItem(1, 40, 1),
            new PackageItem(2, 40, 100)
        };
        package.AddPossibleItems(packageItems);
        package.Pack();

        var count = package.PackedItems.Count();

        Assert.Equal<int>(1, count);
        Assert.Equal<decimal>(100, package.PackedItems.First().Price);
    }

    [Fact]
    public void Pack_AddTwoItemsFillingPackToCapacity_Returns2PackedItems()
    {
        var package = new Package(80);
        var packageItems = new List<PackageItem>()
        {
            new PackageItem(1, 40, 100),
            new PackageItem(2, 40, 1)
        };
        package.AddPossibleItems(packageItems);
        package.Pack();

        Assert.Equal<int>(2, package.PackedItems.Count());
    }

    [Fact]
    public void Pack_OnePackageItemOverPriced_Returns1PackedItem()
    {
        var package = new Package(80);
        var packageItems = new List<PackageItem>()
        {
            new PackageItem(1, 40, 100000),
            new PackageItem(2, 40, 1)
        };
        package.AddPossibleItems(packageItems);
        package.Pack();

        var count = package.PackedItems.Count();
        Assert.Equal<int>(1, count);
    }

    [Fact]
    public void Pack_AddSameWeightAndPrice_ReturnsExcludesItems9And10AndItemCountIs8()
    {
        var package = new Package(100);
        var packageItems = new List<PackageItem>()
        {
            new PackageItem(1, 20, 20),
            new PackageItem(2, 10, 10),
            new PackageItem(3, 5, 5),
            new PackageItem(4, 4, 4),
            new PackageItem(5, 3, 3),
            new PackageItem(6, 2, 2),
            new PackageItem(7, 1, 1),
            new PackageItem(8, 18, 18),
            new PackageItem(9, 48, 40),
            new PackageItem(10, 50, 50),

        };
        package.AddPossibleItems(packageItems);
        package.Pack();

        var invalidItems = package.PackedItems.Where(x => x.Index == 9 || x.Index == 10).Count();

        Assert.Equal<int>(0, invalidItems);
        Assert.Equal<int>(8, package.PackedItems.Count());
    }

    [Fact]
    public void Pack_Add20SmallPackages_Returns15()
    {
        var package = new Package(100);
        var packageItems = new List<PackageItem>()
        {
            new PackageItem(1, 1, 1),
            new PackageItem(2, 1, 1),
            new PackageItem(3, 1, 1),
            new PackageItem(4, 1, 1),
            new PackageItem(5, 1, 1),
            new PackageItem(6, 1, 1),
            new PackageItem(7, 1, 1),
            new PackageItem(8, 1, 1),
            new PackageItem(9, 1, 1),
            new PackageItem(10, 1, 1),
            new PackageItem(11, 1, 1),
            new PackageItem(12, 1, 1),
            new PackageItem(13, 1, 1),
            new PackageItem(14, 1, 1),
            new PackageItem(15, 1, 1),
            new PackageItem(16, 1, 1),
            new PackageItem(17, 1, 1),
            new PackageItem(18, 1, 1),
            new PackageItem(19, 1, 1),
            new PackageItem(20, 1, 1)
        };
        package.AddPossibleItems(packageItems);
        package.Pack();

        Assert.Equal<int>(15, package.PackedItems.Count());
    }

    [Fact]
    public void ToString_TwoSmallPackagesAndOneEmpty_Returns1Comma2()
    {
        var package = new Package(100);
        var packageItems = new List<PackageItem>()
        {
            new PackageItem(1, 20, 20),
            new PackageItem(2, 10, 10)

        };
        package.AddPossibleItems(packageItems);
        package.Pack();

        var result = package.ToString();

        Assert.Equal("1,2", result);
    }

    [Fact]
    public void ToString_EmptyPackage_ReturnsDash()
    {
        var packages = new List<Package>();
        var package = new Package(1);
        var packageItems = new List<PackageItem>()
        {
            new PackageItem(1, 20, 20),
            new PackageItem(2, 10, 10),
            new PackageItem(3, 70, 10),
        };
        package.AddPossibleItems(packageItems);
        package.Pack();

        var result = package.ToString();

        Assert.Equal("-", result);
    }

    [Fact]
    public void Pack_PackageExceedsMaximumLimit_ThrowsInvalidPackageOperation()
    {
        var package = new Package(101);
        var packageItems = new List<PackageItem>()
        {
            new PackageItem(1, 20, 100),
            new PackageItem(2, 40, 1)
        };
        package.AddPossibleItems(packageItems);

        var exception = Assert.Throws<APIException>(() => package.Pack());

        Assert.Equal<ErrorCodes>(ErrorCodes.InvalidPackageOperation, exception.Type);
        Assert.Equal("A package cannot have a weight of: 101", exception.Message);
    }

    [Fact]
    public void Pack_PackageInvalidWeight_ThrowsInvalidPackageOperation()
    {
        var package = new Package(-1);
        var packageItems = new List<PackageItem>()
        {
            new PackageItem(1, 20, 100),
            new PackageItem(2, 40, 1)
        };
        package.AddPossibleItems(packageItems);

        var exception = Assert.Throws<APIException>(() => package.Pack());

        Assert.Equal<ErrorCodes>(ErrorCodes.InvalidPackageOperation, exception.Type);
        Assert.Equal("A package cannot have a weight of: -1", exception.Message);
    }
}