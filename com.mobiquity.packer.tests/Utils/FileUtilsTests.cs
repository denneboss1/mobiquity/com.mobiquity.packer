using com.mobiquity.packer.Enums;
using com.mobiquity.packer.Exceptions;
using com.mobiquity.packer.Utils;

namespace com.mobiquity.packer.tests.Utils;

public class FileUtilsTests : IDisposable
{
    private const string TestFilePath = "test.txt";
    private const string TestFileContent = "Hello world";
    public FileUtilsTests()
    {
        File.WriteAllText(TestFilePath, TestFileContent);
    }

    public void Dispose()
    {
        if (File.Exists(TestFilePath))
        {
            File.Delete(TestFilePath);
        }
    }

    [Fact]
    public void ReadUtfFile_ValidPath_ReturnsFileContent()
    {
        FileInfo fi = new FileInfo(TestFilePath);

        var result = FileUtils.ReadUtfFile(fi.FullName);

        Assert.Equal(TestFileContent, result);
    }

    [Fact]
    public void ReadUtfFile_NonUnixPath_ReturnsFileContent()
    {
        FileInfo fi = new FileInfo(TestFilePath);
        
        var filePath = fi.FullName;
        var pathSeparator = "\\";
        var pathParts = filePath.Split(Path.DirectorySeparatorChar);
        var nonUnixPath = string.Join(pathSeparator.ToString(), pathParts);

        var result = FileUtils.ReadUtfFile(nonUnixPath);

        Assert.Equal(TestFileContent, result);
    }

    [Fact]
    public void ReadUtfFile_FileNotFound_ThrowsException()
    {
        var invalidFilePath = "invalid.txt";

        var exception = Assert.Throws<APIException>(() => FileUtils.ReadUtfFile(invalidFilePath));

        Assert.Equal<ErrorCodes>(ErrorCodes.FileNotFound, exception.Type);
    }

    [Fact]
    public void ReadUtfFile_NullOrEmptyFilePath_ThrowsException()
    {
        string path = "";
        var exception = Assert.Throws<APIException>(() => FileUtils.ReadUtfFile(path));

        Assert.Equal<ErrorCodes>(ErrorCodes.FileNotFound, exception.Type);
    }
}