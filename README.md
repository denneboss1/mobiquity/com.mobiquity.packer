# com.mobiquity.packer

com.mobiquity.packer is a **.NET** library that will assist you in determining which items to pack for the least weight and most profit.


### Quick Start

Registry setup:

````bash
nuget source Add -Name "GitLab" -Source "https://gitlab.com/api/v4/projects/43758399/packages/nuget/index.json" -UserName <your_username> -Password <your_token>
````

Installation:

````bash
nuget install com.mobiquity.packer -Source "GitLab"
````

### Default Configuration

````json
{
    "CultureInfo": {
        "GetCulture": "en-IE"
    },
    "Package": {
        "MaxWeight": 100,
        "MaxItems": 15
    },
    "PackageItem": {
        "MaxCost": 100,
        "MaxWeight": 100
    }
}
````